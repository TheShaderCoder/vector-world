package com.openglshaders.src;
import com.javarpg.twod.texture.Texture;

public class Material {
	private Texture texture;
	
	public Material(Texture texture) {
		this.texture = texture;
	}
	
	public Texture getTexture() {
		return texture;
	}
}