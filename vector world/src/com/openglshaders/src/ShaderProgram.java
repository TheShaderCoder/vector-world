package com.openglshaders.src;
import static org.lwjgl.opengl.GL20.*;

import java.io.File;

import org.lwjgl.opengl.GL20;

/**
 * Loads a shader, and stores a reference to it.
 * @author OpenGLShaders
 */
public class ShaderProgram {
	private int program = 0;
	
	public ShaderProgram(File vertexShaderFile, File fragmentShaderFile) {
		int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		program = glCreateProgram();
		
		//Upload the source to the shader
		GL20.glShaderSource(vertexShader, FileUtils.getContentsOfFileAsString(vertexShaderFile));
		GL20.glShaderSource(fragmentShader, FileUtils.getContentsOfFileAsString(fragmentShaderFile));
		
		//Compile the shaders
		GL20.glCompileShader(vertexShader);
		GL20.glCompileShader(fragmentShader);
		
		//Attach the shaders to the program
		GL20.glAttachShader(program, vertexShader);
		GL20.glAttachShader(program, fragmentShader);
		
		//Link and validate the program
		GL20.glLinkProgram(program);
		GL20.glValidateProgram(program);
		
		//Print out the shader log
		System.out.println("Vertex Shader Log: " + glGetShaderInfoLog(vertexShader, 512));
		System.out.println("Fragment Shader Log: " + glGetShaderInfoLog(fragmentShader, 512));
		
		//Get rid of the vertex and fragment shaders which aren't
		//needed anymore now that we have our finished program
		GL20.glDeleteShader(vertexShader);
		GL20.glDeleteShader(fragmentShader);
	}
	
	public int getShaderProgram() {
		return program;
	}
	
	public void bind() {
		glUseProgram(program);
	}
	
	public void unbind() {
		glUseProgram(0);
	}
}