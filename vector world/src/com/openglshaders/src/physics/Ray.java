package com.openglshaders.src.physics;

import org.lwjgl.util.vector.Vector3f;

public class Ray {
	public Vector3f origin;
	public Vector3f direction;
	
	public Ray(Vector3f origin, Vector3f direction) {
		this.origin = origin;
		this.direction = direction;
	}
	
	public Vector3f pointOnRay(float t) {
		Vector3f dir = new Vector3f(direction);
		dir.scale(t);
		Vector3f.add(dir, origin, dir);
		
		return dir; 
	}
}