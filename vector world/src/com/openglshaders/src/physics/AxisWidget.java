package com.openglshaders.src.physics;

import org.lwjgl.util.vector.Vector3f;

public class AxisWidget {
	public Vector3f up;
	public Vector3f right;
	public Vector3f forward;
	
	public AxisWidget() {
		up = new Vector3f(0, 10, 0);
		right = new Vector3f(0, 0, 10);
		forward = new Vector3f(10, 0, 0);
	}
	
	public AxisWidget(Vector3f up, Vector3f forward, Vector3f right) {
		this.up = new Vector3f(up);
		this.right = new Vector3f(right);
		this.forward = new Vector3f(forward);
		this.up.scale(10);
		this.right.scale(10);
		this.forward.scale(10);
	}
	
	public void draw() {
		DebugRenderer.drawLine(new Vector3f(), up, 0, 1, 0);
		DebugRenderer.drawLine(new Vector3f(), forward, 1, 0, 0);
		DebugRenderer.drawLine(new Vector3f(), right, 0, 0, 1);
	}
}