package com.openglshaders.src.physics;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector3f;

public class MatrixMath {
	public static Matrix3f rotate(Matrix3f mat, Vector3f axis, float degrees)
	{			
		Matrix3f temp = new Matrix3f();
		
		float c = (float) Math.cos(Math.toRadians(degrees));
		float s = (float) Math.sin(Math.toRadians(degrees));

		temp.m00 = c + (1 - c) * axis.x * axis.x;
		temp.m01 = (1 - c) * axis.x * axis.y - s * axis.z;
		temp.m02 = (1 - c) * axis.x * axis.z + s * axis.y;
		
		temp.m10 = (1 - c) * axis.x * axis.y - s * axis.z;
		temp.m11 = c + (1 - c) * axis.y * axis.y;
		temp.m12 = (1 - c) * axis.y * axis.z + s * axis.x;
		
		temp.m20 = (1 - c) * axis.x * axis.z + s * axis.y;
		temp.m21 = (1 - c) * axis.y * axis.z + s * axis.x;
		temp.m22 = c + (1 - c) * axis.z * axis.z;
		
		mat = Matrix3f.mul(mat, temp, mat);
		return mat;
	}
	
	public static Matrix3f getXMatrix(float x) {
		Matrix3f temp = new Matrix3f();
		temp.setIdentity();
		
		float sin = (float) Math.sin(Math.toRadians(x));
		float cos = (float) Math.cos(Math.toRadians(x));
		
		temp.m11 = cos;
		temp.m12 = -sin;
		temp.m21 = sin;
		temp.m22 = cos;
		
		return temp;
	}
	
	public static Matrix3f getYMatrix(float y) {
		Matrix3f temp = new Matrix3f();
		temp.setIdentity();
		
		float sin = (float) Math.sin(Math.toRadians(y));
		float cos = (float) Math.cos(Math.toRadians(y));
		
		temp.m00 = cos;
		temp.m02 = sin;
		temp.m20 = -sin;
		temp.m22 = cos;
		
		return temp;
	}
	
	public static Matrix3f getZMatrix(float z) {
		Matrix3f temp = new Matrix3f();
		temp.setIdentity();
		
		float sin = (float) Math.sin(Math.toRadians(z));
		float cos = (float) Math.cos(Math.toRadians(z));
		
		temp.m00 = cos;
		temp.m01 = -sin;
		temp.m10 = sin;
		temp.m11 = cos;
		
		return temp;
	}
	
	public static Matrix3f rotationMatrixFromEulerAngles(Vector3f rotation) {
		Matrix3f X = getXMatrix(rotation.x);
		Matrix3f Y = getYMatrix(rotation.y);
		Matrix3f Z = getZMatrix(rotation.z);
		
		Matrix3f temp = Matrix3f.mul(X, Y, null);
		temp = Matrix3f.mul(temp, Z, null);
		return temp;
	}
	
	public static void main(String args[]) {
		Matrix3f temp = rotationMatrixFromEulerAngles(new Vector3f((float) Math.toDegrees(-2.6337), (float) Math.toDegrees(-0.47158), (float) Math.toDegrees(-1.2795)));
		temp.transpose();
		System.out.println(temp);
	}
}