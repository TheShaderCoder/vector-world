package com.openglshaders.src.physics;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.util.vector.Vector3f;

public class DebugRenderer {

	public static void drawRay(Ray ray) 
	{
		drawRay(ray, 1, 0, 0);
	}
	
	public static void drawRay(Ray ray, float r, float g, float b) 
	{
		glLineWidth(2.5f);
		glBegin(GL_LINES);
		glColor3f(r, g, b);
			glVertex3f(ray.origin.x, ray.origin.y, ray.origin.z);
			glVertex3f(ray.origin.x + ray.direction.x, ray.origin.y + ray.direction.y, ray.origin.z + ray.direction.z);
		glEnd();
		glColor3f(r, g, b);
		glPointSize(10);
		glBegin(GL_POINTS);
			glVertex3f(ray.origin.x, ray.origin.y, ray.origin.z);
		glEnd();
		glColor3f(1.0F, 1.0F, 1.0F);
		glPointSize(0);
		glLineWidth(0);
	}
	
	public static void drawLine(Vector3f start, Vector3f end) 
	{
		drawLine(start, end, 1.0F, 1.0F, 1.0F);
	}
	
	public static void drawLine(Vector3f start, Vector3f end, float r, float g, float b) 
	{
		glColor3f(r, g, b);
		glLineWidth(2);
		glBegin(GL_LINES);
			glVertex3f(start.x, start.y, start.z);
			glVertex3f(end.x, end.y, end.z);
		glEnd();
		glColor3f(1, 1, 1);
		glLineWidth(0);
	}
	
	public static void drawPoint(Vector3f v) {
		drawPoint(v, 1.0F, 1.0F, 1.0F);
	}
	
	public static void drawPoint(Vector3f v, float r, float g, float b) {
		glColor3f(r, g, b);
		glPointSize(10);
		glBegin(GL_POINTS);
			glVertex3f(v.x, v.y, v.z);
		glEnd();
		glColor3f(1, 1, 1);
		glPointSize(0);
	}
	
	public static void drawDebugTriangle(Vector3f v0, Vector3f v1, Vector3f v2, boolean hit)
	{
		glBegin(GL_TRIANGLES);
		if (hit)
		{
			glColor3f(0.0F, 1.0F, 0.0F);			
		}
		else
		{
			glColor3f(1.0F, 0.0F, 0.0F);
		}
		
		glVertex3f(v0.x, v0.y, v0.z);
		glVertex3f(v1.x, v1.y, v1.z);
		glVertex3f(v2.x, v2.y, v2.z);
		
		glColor3f(1.0F, 1.0F, 1.0F);
		glEnd();
	}
}