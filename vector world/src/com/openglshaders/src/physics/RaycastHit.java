package com.openglshaders.src.physics;

import org.lwjgl.util.vector.Vector3f;

public class RaycastHit
{
	public boolean hit;
	public Vector3f hitLocation;
	
	public RaycastHit(boolean hit, Vector3f hitLocation) {
		this.hit = hit;
		this.hitLocation = hitLocation;
	}
} 