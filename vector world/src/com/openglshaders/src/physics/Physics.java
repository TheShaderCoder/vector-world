package com.openglshaders.src.physics;

import org.lwjgl.util.vector.Matrix2f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.openglshaders.src.object.Sphere;

public class Physics {
	
	public static final Vector3f up = new Vector3f(0, 1, 0);
	public static final Vector3f down = new Vector3f(0, -1, 0);
	public static final Vector3f right = new Vector3f(1, 0, 0);
	public static final Vector3f left = new Vector3f(-1, 0, 0);
	public static final Vector3f forward = new Vector3f(0, 0, 1);
	public static final Vector3f backward = new Vector3f(0, 0, -1);

	/**
	 * Returns a RaycastHit with the information about the intersection.
	 * I used the formulas in Mathematics for 3D Game Programming and Computer Graphics, pages 139-142.
	 * @param ray
	 * @param p0
	 * @param p1
	 * @param p2
	 * 
	 * @return
	 */
	public static boolean testRayTriangle(Ray ray, Vector3f p0, Vector3f p1, Vector3f p2, RaycastHit hit) 
	{			
		Vector3f Q1 = Vector3f.sub(p1, p0, null);
		Vector3f Q2 = Vector3f.sub(p2, p0, null);
		
		Vector3f normal = Vector3f.cross(Q1, Q2, null);
		float d = -Vector3f.dot(normal, p0);
		Vector4f L = new Vector4f(normal.x, normal.y, normal.z, d);
		float LDotV = (Vector4f.dot(L, new Vector4f(ray.direction.x, ray.direction.y, ray.direction.z, 0)));
		float t = -((Vector4f.dot(L, new Vector4f(ray.origin.x, ray.origin.y, ray.origin.z, 1))) / LDotV);
		
		Vector3f P = ray.pointOnRay(t);

		if (hit != null)
		{
			hit.hit = false;
			hit.hitLocation = P;
		}
		
		if (LDotV <= 0) 
		{	
			return false;
		}
		
		Vector3f R = Vector3f.sub(P, p0, null);
				
		float Q1DOTQ2 = Vector3f.dot(Q1, Q2);
		
		float multiplier = 1.0F / ( (square(Q1) * square(Q2)) - (square(Q1DOTQ2)) );
		Matrix2f mat1 = new Matrix2f();
		mat1.setZero();
		mat1.m00 = square(Q2);
		mat1.m10 = -Q1DOTQ2;
		mat1.m01 = -Q1DOTQ2;
		mat1.m11 = square(Q1);
		
		Vector2f pointsMat = new Vector2f(Vector3f.dot(R, Q1), Vector3f.dot(R, Q2));
		
		Vector2f weights = new Vector2f();
		weights = (Vector2f) Matrix2f.transform(mat1, pointsMat, null).scale(multiplier);
		
		float w1 = weights.x;
		float w2 = weights.y;
		float w0 = 1 - w1 - w2;
		
		if (w0 >= 0 && w1 >= 0 && w2 >= 0) 
		{
			if (hit != null)
			{
				hit.hit = true;
			}
			return true;
		}
		return false;
	}
	
	public static boolean testRaySphere(Ray ray, Sphere sphere, RaycastHit hit) {
		float a = square(ray.direction);
		float b = 2 * (Vector3f.dot(ray.origin, ray.direction));
		float c = square(ray.origin) - square(sphere.radius);
		
		float discriminant = square(b) - (4 * a * c);
		float t = (-b - (float) Math.sqrt(discriminant)) / 
							(2 * a);
		hit.hit = false;
		hit.hitLocation.set(0, 0, 0);
		
		if (discriminant < 0) {
			return false;
		} else if (discriminant == 0) {
			hit.hit = true;
			hit.hitLocation = ray.pointOnRay(t);
			return true;
		} else if (discriminant > 0) {
			hit.hit = true;
			hit.hitLocation = ray.pointOnRay(t);
			return true;
		}
		
		return false;
	}
	
	public static float square(Vector3f vec) {
		return Vector3f.dot(vec, vec);
	}
	
	public static float square(float f) {
		return f * f;
	}
}