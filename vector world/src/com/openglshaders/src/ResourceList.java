package com.openglshaders.src;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.javarpg.twod.texture.Texture;
import com.openglshaders.src.model.Mesh;

/**
 * Messy, unfinished code.
 * @author administrator
 *
 */
public class ResourceList {
	private static Map<String, ShaderProgram> shaders = new HashMap<String, ShaderProgram>();
	private static Map<String, Mesh> meshes = new HashMap<String, Mesh>();
	private static Map<String, Texture> textures = new HashMap<String, Texture>();

	private static boolean loaded;
	
	public static Texture getTexture(String key) {
		checkLoad();
		return textures.get(key);
	}
	
	public static Mesh getMesh(String key) {
		checkLoad();
		return meshes.get(key);
	}
	
	public static ShaderProgram getShader(String key) {
		checkLoad();
		return shaders.get(key);
	}
	
	public static void loadShader(String name, File vertexShaderFile, File fragmentShaderFile) {
		if (!shaders.containsKey(name)) {
			ShaderProgram program = new ShaderProgram(vertexShaderFile, fragmentShaderFile);
			shaders.put(name, program);			
		}
	}
	
	public static void addMesh(String name, Mesh mesh) {
		checkLoad();
		if (!meshes.containsKey(name)) {
			meshes.put(name, mesh);
		}
	}
	
	public static boolean hasMesh(String name) {
		checkLoad();
		return meshes.containsKey(name);
	}
	
	public static void loadMesh(String name, File file) {
		if (!meshes.containsKey(name)) {
			Mesh mesh = null;
			try {
				mesh = Mesh.loadFromObj(file);
				meshes.put(name, mesh);			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void setMesh(String name, Mesh mesh) {
		checkLoad();
		if (hasMesh(name)) {
			meshes.put(name, mesh);
		}
	}
	
	private static void checkLoad() {
		if (!loaded) {
			load();
		}
	}
	
	public static void load() {
		loadMesh("cube", new File("assets/obj/cube2.obj"));
		loadShader("tex_shader", new File("assets/shaders/tex_shader.vs"), new File("assets/shaders/tex_shader.fs"));
		loaded = true;
	}
}