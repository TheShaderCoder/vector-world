package com.openglshaders.src;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class FileUtils {
	/**
	 * Returns a string representation of a file
	 * @param file
	 * @return
	 */
	public static String getContentsOfFileAsString(File file) {
		if (!validFile(file)) {
			return null;
		}
		
		StringBuilder fileContents = new StringBuilder();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));

			String line = reader.readLine();
			while (line != null) {
				fileContents.append(line + "\n");
				line = reader.readLine();
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();	
		}

		return fileContents.toString();
	}
	
	/**
	 * Returns an array of strings that represent each line of a file.
	 * @param file
	 * @return
	 */	
	public static ArrayList<String> getContentsOfFileAsArray(File file) {
		if (!validFile(file)) {
			return null;
		}
		
		ArrayList<String> lines = new ArrayList<String>();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));

			String line = reader.readLine();
			while (line != null) {
				lines.add(line);
				line = reader.readLine();
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();	
		}

		return lines;
	}
	
	private static boolean validFile(File file) {
		return (file != null && file.isFile());
	}
	
	public static void combineTexturesIntoSpriteSheet(File inputFiles[], int tileSize, int spriteSheetSize, File outputPath) {
		BufferedImage[] tileImages = new BufferedImage[inputFiles.length];
		
		try {
			for (int i = 0; i < inputFiles.length; i++) {
				tileImages[i] = ImageIO.read(inputFiles[i]);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	
		BufferedImage image = new BufferedImage(spriteSheetSize, spriteSheetSize, BufferedImage.TYPE_INT_ARGB);
		int tileCapacity = (int) (spriteSheetSize / tileSize);
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				int tileX = (int) (i / tileSize);
				int tileY = (int) (j / tileSize);
				int fileIndex = tileX * tileCapacity + tileY;
				
				if (fileIndex < tileImages.length) {
					image.setRGB(i, j, tileImages[fileIndex].getRGB(i - (tileX * tileSize), j - (tileY * tileSize)));					
				} else {
					image.setRGB(i, j, 0xFFFFFF);
				}
			}	
		}
		
		try {
			ImageIO.write(image, "png", outputPath);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public static void main(String args[]) {
		File files[] = new File[95];
		
		for (int i = 0; i < 95; i++) {
			files[i] = new File("./assets/tile/" + i + ".png");
		}
		
		combineTexturesIntoSpriteSheet(files, 96, 1024, new File("./assets/tile/spritesheet.png"));
	}
}