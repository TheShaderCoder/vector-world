package com.openglshaders.src.object;

import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;

public class Object3D {
	private Mesh mesh = null;
	
	public Object3D() {
		
	}
	
	public Object3D(Mesh mesh) {
		this.mesh = mesh;
	}
	
	public Mesh getMesh() {
		return mesh;
	}
	
	protected static void offsetData(float srcData[], Vector3f origin) {
		for (int i = 0; i < srcData.length; i += 8) {
			srcData[i] -= origin.x;
			srcData[i + 1] -= origin.y;
			srcData[i + 2] -= origin.z;
		}
	}
}