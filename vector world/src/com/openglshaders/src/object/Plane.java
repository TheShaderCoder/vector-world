package com.openglshaders.src.object;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;

public class Plane extends Object3D {
	private Mesh mesh;
	
	public Plane(Vector3f origin, Vector2f dimensions) {
		mesh = ShapeGenerator.createPlaneMesh(origin, dimensions);
	}
	
	@Override
	public Mesh getMesh() {
		return mesh;
	}
}