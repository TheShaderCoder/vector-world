package com.openglshaders.src.object;


import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;

public class Cube extends Object3D {

	public Vector3f position = new Vector3f();
	public Mesh mesh;
	
	public Cube(Vector3f origin, Vector3f dimensions) {
		mesh = ShapeGenerator.createBoxMesh(origin, dimensions);
	}

	public Cube(Vector3f origin, Vector3f dimensions, Vector3f position) {
		this(origin, dimensions);
		this.position = position;
	}
	
	@Override
	public Mesh getMesh() {
		return mesh;
	}
}