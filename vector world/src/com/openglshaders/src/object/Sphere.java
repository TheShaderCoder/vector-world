package com.openglshaders.src.object;

import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;

public class Sphere extends Object3D {
	private Vector3f position = new Vector3f();
	public float radius;
	
	private Mesh mesh;
	
	public Sphere(Vector3f origin, float radius, int subdivisions) {
		mesh = ShapeGenerator.createSphereMesh(origin, radius, subdivisions);
		this.radius = radius;
	}
	
	@Override
	public Mesh getMesh() {
		return mesh;
	}
}