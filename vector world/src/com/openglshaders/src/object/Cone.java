package com.openglshaders.src.object;

import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;

public class Cone extends Object3D {
	private Mesh mesh;
	
	public Cone(Vector3f origin, int rings, float radius, float height) {
		mesh = ShapeGenerator.createConeMesh(origin, rings, radius, height);
	}
	
	@Override
	public Mesh getMesh() {
		return mesh;
	}
}