package com.openglshaders.src.object;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;
import com.openglshaders.src.model.MeshBuilder;
import com.openglshaders.src.model.Vertex;


public class ShapeGenerator {
	
	public static Mesh createPlaneMesh(Vector3f origin, Vector2f dimensions) {
		float hx = dimensions.x / 2.0F;
		float hz = dimensions.y / 2.0F;
		
		MeshBuilder builder = new MeshBuilder();
		builder.startMeshBuild();
		
		//(VxVyVz, NxNyNz, TsTt)
		builder.putVertex(new Vertex(hx, 0, -hz, 0, 1, 0, 1, 0));
		builder.putVertex(new Vertex(-hx, 0, hz, 0, 1, 0, 0, 1));
		builder.putVertex(new Vertex(-hx, 0, -hz, 0, 1, 0, 0, 0));
		builder.putVertex(new Vertex(hx, 0, -hz, 0, 1, 0, 1, 0));
		builder.putVertex(new Vertex(hx, 0, hz, 0, 1, 0, 1, 1));
		builder.putVertex(new Vertex(-hx, 0, hz, 0, 1, 0, 0, 1));
		
		builder.setOrigin(origin);		
		
		return builder.endMeshBuild();
	}
	
	public static Mesh createBoxMesh(Vector3f origin, Vector3f dimensions) {
		float hx = dimensions.x / 2.0F;
		float hy = dimensions.y / 2.0F;
		float hz = dimensions.z / 2.0F;
		
		//(VxVyVz, NxNyNz, TsTt)
		MeshBuilder builder = new MeshBuilder();
		
		builder.startMeshBuild();
		
		//Bottom 
		builder.putVertex(new Vertex(-hx, -hy, -hz, 0, -1, 0, 1, 1));
		builder.putVertex(new Vertex(hx, -hy, hz, 0, -1, 0, 0, 0));
		builder.putVertex(new Vertex(hx, -hy, -hz, 0, -1, 0, 0, 1));
		builder.putVertex(new Vertex(-hx, -hy, -hz, 0, -1, 0, 1, 1));
		builder.putVertex(new Vertex(-hx, -hy, hz, 0, -1, 0, 1, 0));
		builder.putVertex(new Vertex(hx, -hy, hz, 0, -1, 0, 0, 0));

		//Top
		builder.putVertex(new Vertex(-hx, hy, -hz, 0, 1, 0, 1, 1));
		builder.putVertex(new Vertex(hx, hy, -hz, 0, 1, 0, 0, 1));
		builder.putVertex(new Vertex(hx, hy, hz, 0, 1, 0, 0, 0));
		builder.putVertex(new Vertex(-hx, hy, -hz, 0, 1, 0, 1, 1));
		builder.putVertex(new Vertex(hx, hy, hz, 0, 1, 0, 0, 0));
		builder.putVertex(new Vertex(-hx, hy, hz, 0, 1, 0, 1, 0));
		
		//Left
		builder.putVertex(new Vertex(-hx, -hy, -hz, 0, 0, -1, 1, 1));
		builder.putVertex(new Vertex(hx, hy, -hz, 0, 0, -1, 0, 0));
		builder.putVertex(new Vertex(-hx, hy, -hz, 0, 0, -1, 1, 0));
		builder.putVertex(new Vertex(-hx, -hy, -hz, 0, 0, -1, 1, 1));
		builder.putVertex(new Vertex(hx, -hy, -hz, 0, 0, -1, 0, 1));
		builder.putVertex(new Vertex(hx, hy, -hz, 0, 0, -1, 0, 0));
		
		//Right
		builder.putVertex(new Vertex(-hx, -hy, hz, 0, 0, 1, 0, 1));
		builder.putVertex(new Vertex(-hx, hy, hz, 0, 0, 1, 0, 0));
		builder.putVertex(new Vertex(hx, hy, hz, 0, 0, 1, 1, 0));
		builder.putVertex(new Vertex(-hx, -hy, hz, 0, 0, 1, 0, 1));
		builder.putVertex(new Vertex(hx, hy, hz, 0, 0, 1, 1, 0));
		builder.putVertex(new Vertex(hx, -hy, hz, 0, 0, 1, 1, 1));
		
		//Back
		builder.putVertex(new Vertex(-hx, -hy, -hz, 1, 0, 0, 0, 1));
		builder.putVertex(new Vertex(-hx, hy, -hz, 1, 0, 0, 0, 0));
		builder.putVertex(new Vertex(-hx, hy, hz, 1, 0, 0, 1, 0));
		builder.putVertex(new Vertex(-hx, -hy, -hz, 1, 0, 0, 0, 1));
		builder.putVertex(new Vertex(-hx, hy, hz, 1, 0, 0, 1, 0));
		builder.putVertex(new Vertex(-hx, -hy, hz, 1, 0, 0, 1, 1));
		
		//Front
		builder.putVertex(new Vertex(hx, -hy, hz, -1, 0, 0, 0, 1));
		builder.putVertex(new Vertex(hx, hy, -hz, -1, 0, 0, 1, 0));
		builder.putVertex(new Vertex(hx, -hy, -hz, -1, 0, 0, 1, 1));
		builder.putVertex(new Vertex(hx, -hy, hz, -1, 0, 0, 0, 1));
		builder.putVertex(new Vertex(hx, hy, hz, -1, 0, 0, 0, 0));
		builder.putVertex(new Vertex(hx, hy, -hz, -1, 0, 0, 1, 0));
	
		builder.setOrigin(origin);
		
		return builder.endMeshBuild();
	}

	public static Mesh createConeMesh(Vector3f origin, int rings, float radius, float height) {
		MeshBuilder builder = new MeshBuilder();
		
		builder.startMeshBuild();
		
		float angleOffset = (float) Math.toRadians(360.0F / rings);
		for (int i = 0; i < rings; i++) {
			builder.putVertex(new Vertex(0, 0, 0, 0, -1, 0, 0, 0));
			builder.putVertex(new Vertex(radius * (float) (Math.cos(angleOffset * (float) i)), 0, radius * (float) (Math.sin(angleOffset * (float) i)),
													 0, -1, 0, 0, 0));
			builder.putVertex(new Vertex(radius * (float) (Math.cos(angleOffset * (float) (i + 1))), 0, 
													 radius * (float) (Math.sin(angleOffset * (float) (i + 1))), 0, -1, 0, 0, 0));
			builder.putVertex(new Vertex(radius * (float) (Math.cos(angleOffset * (float) i)), 0, 
													 radius * (float) (Math.sin(angleOffset * (float) i)), 0, -1, 0, 0, 0));
			builder.putVertex(new Vertex(radius * (float) (Math.cos(angleOffset * (float) (i + 1))), 0, 
													 radius * (float) (Math.sin(angleOffset * (float) (i + 1))), 0, -1, 0, 0, 0));
			builder.putVertex(new Vertex(0, height, 0, 0, -1, 0, 0, 0));
		}
		builder.setOrigin(origin);
		
		return builder.endMeshBuild();
	}
	
	public static Mesh createCylinderMesh(Vector3f origin, float radius, float height, int rings) {
		MeshBuilder builder = new MeshBuilder();
		
		builder.startMeshBuild();
		
		float ringOffset = (float) Math.toRadians(360.0F / (float) rings);
		
		for (int i = 0; i < rings; i++) {
			//Bottom triangle
			builder.putVertex(new Vertex(0, 0, 0, 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * (i + 1)), 0, (float) Math.sin(ringOffset * (i + 1)), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * i), 0, (float) Math.sin(ringOffset * i), 0, 0, 0, 0, 0));
			
			//Side triangles
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * i), 0, (float) Math.sin(ringOffset * i), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * (i + 1)), 0, (float) Math.sin(ringOffset * (i + 1)), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * i), height, (float) Math.sin(ringOffset * i), 0, 0, 0, 0, 0));
			
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * (i + 1)), 0, (float) Math.sin(ringOffset * (i + 1)), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * (i + 1)), height, (float) Math.sin(ringOffset * (i + 1)), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * i), height, (float) Math.sin(ringOffset * i), 0, 0, 0, 0, 0));

			//Top triangle
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * i), height, (float) Math.sin(ringOffset * i), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex((float) Math.cos(ringOffset * (i + 1)), height, (float) Math.sin(ringOffset * (i + 1)), 0, 0, 0, 0, 0));
			builder.putVertex(new Vertex(0, height, 0, 0, 0, 0, 0, 0));
		}
		
		builder.setOrigin(origin);
		
		return builder.endMeshBuild();
	}
	/*
	public static Mesh createSphereMesh(Vector3f origin, float radius, int horizontalRings, int verticalRings) {
		MeshBuilder builder = new MeshBuilder();
		
		builder.startMeshBuild();
		
		float hRingOffset = (float) Math.toRadians(360.0F / (float) horizontalRings);
		float vRingOffset = (float) Math.toRadians(360.0F / (float) verticalRings);
		
		for (int i = 0; i < verticalRings; i++) {
			for (int j = 0; j < horizontalRings; j++) {
				float thetaV = vRingOffset * i;
				float thetaH = hRingOffset * j;
				
				float x = (float) Math.sin(thetaH) * (float) Math.cos(thetaV);
				float y = (float) Math.sin(thetaH) * (float) Math.sin(thetaV);
				float z = (float) Math.cos(thetaH);
				
				builder.putVertex(new Vertex(x, y, z, 0, 0, 0, 0, 0));
			}
		}
		
		builder.setOrigin(origin);
		return builder.endMeshBuild();
	}
	*/
	public static Mesh createSphereMesh(Vector3f origin, float radius, int subdivisions) {
		MeshBuilder builder = new MeshBuilder();
		
		builder.startMeshBuild();
		
		float t = (float) ((1.0F + Math.sqrt(5)) / 2.0F);
		Vertex v1 = new Vertex(0, 1, t, 1, 1, 1, 1, 1);
		Vertex v2 = new Vertex(0, 1, -t, 1, 1, 1, 1, 1);
		Vertex v3 = new Vertex(0, -1, t, 1, 1, 1, 1, 1);
		Vertex v4 = new Vertex(0, -1, -t, 1, 1, 1, 1, 1);
		
		Vertex v5 = new Vertex(1, t, 0, 1, 1, 1, 1, 1);
		Vertex v6 = new Vertex(1, -t, 0, 1, 1, 1, 1, 1);
		Vertex v7 = new Vertex(-1, t, 0, 1, 1, 1, 1, 1);
		Vertex v8 = new Vertex(-1, -t, 0, 1, 1, 1, 1, 1);

		
		Vertex v9 = new Vertex(t, 0, 1, 1, 1, 1, 1, 1);
		Vertex v10 = new Vertex(-t, 0, 1, 1, 1, 1, 1, 1);
		Vertex v11 = new Vertex(t, 0, -1, 1, 1, 1, 1, 1);
		Vertex v12 = new Vertex(-t, 0, -1, 1, 1, 1, 1, 1);
		
		IcosahedronFace faces[] = new IcosahedronFace[20];
		
		faces[0] = new IcosahedronFace(v1, v9, v3);
		faces[1] = new IcosahedronFace(v1, v3, v10);
		faces[2] = new IcosahedronFace(v1, v5, v9);
		faces[3] = new IcosahedronFace(v5, v11, v9);
		faces[4] = new IcosahedronFace(v11, v6, v9);
		
		faces[5] = new IcosahedronFace(v9, v6, v3);
		faces[6] = new IcosahedronFace(v11, v4, v6);
		faces[7] = new IcosahedronFace(v5, v2, v11);
		faces[8] = new IcosahedronFace(v11, v2, v4);
		faces[9] = new IcosahedronFace(v2, v12, v4);
		
		faces[10] = new IcosahedronFace(v6, v4, v8);
		faces[11] = new IcosahedronFace(v6, v8, v3);
		faces[12] = new IcosahedronFace(v8, v10, v3);
		faces[13] = new IcosahedronFace(v8, v12, v10);
		faces[14] = new IcosahedronFace(v12, v7, v10);
		
		faces[15] = new IcosahedronFace(v7, v5, v1);
		faces[16] = new IcosahedronFace(v5, v7, v2);
		faces[17] = new IcosahedronFace(v2, v7, v12);
		faces[18] = new IcosahedronFace(v1, v10, v7);
		faces[19] = new IcosahedronFace(v12, v8, v4);
		
		
		for (int i = 0; i < (subdivisions - 1); i++) {
			IcosahedronFace faces2[] = new IcosahedronFace[faces.length * 4];
			
			for (int j = 0; j < faces2.length; j += 4) {
				IcosahedronFace temp[] = faces[(j / 4)].subdivide();
				faces2[j] = temp[0];
				faces2[j + 1] = temp[1];
				faces2[j + 2] = temp[2];
				faces2[j + 3] = temp[3];
			}		
			
			faces = faces2;
		}
		
		for (int i = 0; i < faces.length; i++) {
			faces[i].putVertices(builder);
		}
		
		builder.setOrigin(origin);
		return builder.endMeshBuild();
	}
	
	private static class IcosahedronFace {
		public Vertex v1;
		public Vertex v2;
		public Vertex v3;
		
		public IcosahedronFace(Vertex v1, Vertex v2, Vertex v3) {
			this.v1 = v1;
			this.v2 = v2;
			this.v3 = v3;
		}
		
		public IcosahedronFace[] subdivide() {
			Vertex sv1 = new Vertex((v1.vx + v2.vx) / 2.0F, (v1.vy + v2.vy) / 2.0F, (v1.vz + v2.vz) / 2.0F, 1, 1, 1, 1, 1);
			Vertex sv2 = new Vertex((v2.vx + v3.vx) / 2.0F, (v2.vy + v3.vy) / 2.0F, (v2.vz + v3.vz) / 2.0F, 1, 1, 1, 1, 1);
			Vertex sv3 = new Vertex((v1.vx + v3.vx) / 2.0F, (v1.vy + v3.vy) / 2.0F, (v1.vz + v3.vz) / 2.0F, 1, 1, 1, 1, 1);
			
			IcosahedronFace faces[] = new IcosahedronFace[4];
			
			faces[0] = new IcosahedronFace(v1, sv1, sv3).circlefy();
			faces[1] = new IcosahedronFace(sv1, sv2, sv3).circlefy();
			faces[2] = new IcosahedronFace(sv1, v2, sv2).circlefy();
			faces[3] = new IcosahedronFace(sv3, sv2, v3).circlefy();
			
			return faces;
		}
		
		private IcosahedronFace circlefy() {
			circlefyVertex(v1);
			circlefyVertex(v2);
			circlefyVertex(v3);
			return this;
		}
		
		private void circlefyVertex(Vertex vertex) {
			double length = Math.sqrt(vertex.vx * vertex.vx + vertex.vy * vertex.vy + vertex.vz * vertex.vz);
			vertex.vx /= length;
			vertex.vy /= length;
			vertex.vz /= length;
		}
		
		public void putVertices(MeshBuilder builder) {
			builder.putVertex(v1).putVertex(v2).putVertex(v3);
		}
	}
}