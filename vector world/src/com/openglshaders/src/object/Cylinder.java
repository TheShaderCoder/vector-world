package com.openglshaders.src.object;

import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.model.Mesh;

public class Cylinder extends Object3D {
	private Mesh mesh;
	
	public Cylinder(Vector3f origin, float radius, float height, int rings) {
		mesh = ShapeGenerator.createCylinderMesh(origin, radius, height, rings);
	}
	
	@Override
	public Mesh getMesh() {
		return mesh;
	}
}