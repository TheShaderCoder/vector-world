package com.openglshaders.src.model;

import java.util.HashMap;
import java.util.Map;

public class Model {
	private Map<String, Mesh> meshes = new HashMap<String, Mesh>();
	
	public Model() {
		
	}
	
	public Model addMesh(String name, Mesh mesh) {
		meshes.put(name, mesh);
		return this;
	}
}