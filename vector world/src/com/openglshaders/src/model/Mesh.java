package com.openglshaders.src.model;


import java.io.File;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.openglshaders.src.FileUtils;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

/**
 * Stores data for a 3D model and renders it.
 * Stores the data as VxVyVz/NxNyNz/TsTt.
 * @author OpenGLShaders
 */
public class Mesh {
	private int VBO;
	private ArrayList<Vertex> vertices;
	private int vertexCount = 0;
	
	public Mesh(ArrayList<Vertex> vertices) {
		setupVBO(vertices);
	}
	
	private FloatBuffer getDataBufferFromVertexList(ArrayList<Vertex> vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.size() * 8);
		for (int i = 0; i < vertices.size(); i++) {
			buffer.put(vertices.get(i).vx);
			buffer.put(vertices.get(i).vy);
			buffer.put(vertices.get(i).vz);
			
			buffer.put(vertices.get(i).nx);
			buffer.put(vertices.get(i).ny);
			buffer.put(vertices.get(i).nz);
			
			buffer.put(vertices.get(i).ts);
			buffer.put(vertices.get(i).tt);
		}
		buffer.flip();
		
		return buffer;
	}
	
	private void setupVBO(ArrayList<Vertex> vertices) {
		Object verticesCopy = vertices.clone();
		if (verticesCopy instanceof Vertex) {
			this.vertices = (ArrayList<Vertex>) vertices;			
		}
		
		VBO = glGenBuffers();
		
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, getDataBufferFromVertexList(vertices), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		vertexCount = vertices.size();
	}
	
	public int getVBO() {
		return VBO;
	}
	
	public static Mesh loadFromObj(File file) throws Exception {
		if (file == null || !file.isFile() || !file.exists() || !file.getName().endsWith(".obj")) {
			throw new Exception("The file is not a valid .obj file.");
		}
		
		ArrayList<String> list = FileUtils.getContentsOfFileAsArray(file);
		ArrayList<Vector3f> vertices = new ArrayList<Vector3f>();
		ArrayList<Vector3f> normals = new ArrayList<Vector3f>();
		ArrayList<Vector2f> textureCoords = new ArrayList<Vector2f>();
		ArrayList<TriangleIndexer> triangleIndexers = new ArrayList<TriangleIndexer>();
		
		for (String line : list) {
			if (line.startsWith("v ")) { 
				String numberStrings[] = line.substring(2).split(" ");
				if (numberStrings.length != 3) {
					throw new Exception("Positions must have 3 components.");
				}
				
				vertices.add(new Vector3f(Float.valueOf(numberStrings[0]), 
										  Float.valueOf(numberStrings[1]), 
										  Float.valueOf(numberStrings[2])));
			} else if (line.startsWith("vn ")) {
				String normalStrings[] = line.substring(3).split(" ");
				if (normalStrings.length != 3) {
					throw new Exception("Normals must have 3 components.");
				}
				
				normals.add(new Vector3f(Float.valueOf(normalStrings[0]), 
										 Float.valueOf(normalStrings[1]), 
										 Float.valueOf(normalStrings[2])));
			} else if (line.startsWith("vt ")) {
				String textureStrings[] = line.substring(3).split(" ");
				if (textureStrings.length != 2) {
					throw new Exception("Texture coordinates must have 2 components.");
				}
				
				textureCoords.add(new Vector2f(Float.valueOf(textureStrings[0]), 
										 Float.valueOf(textureStrings[1])));
			} else if (line.startsWith("f ")) {
				TriangleIndexer indexer = new TriangleIndexer();
				
				String points[] = line.substring(2).split(" ");
				if (points.length != 3) {
					throw new Exception("Faces must have 3 points.");
				}
				
				String point0[] = points[0].split("/");
				String point1[] = points[1].split("/");
				String point2[] = points[2].split("/");

				indexer.positionIndices.set(Float.valueOf(point0[0]), Float.valueOf(point1[0]), Float.valueOf(point2[0]));
				indexer.normalIndices.set(Float.valueOf(point0[2]), Float.valueOf(point1[2]), Float.valueOf(point2[2]));
				if (point0[1] != "") {
					indexer.textureIndices.set(Float.valueOf(point0[1]), Float.valueOf(point1[1]), Float.valueOf(point2[1]));					
				}
				triangleIndexers.add(indexer);
			}
		}
		
		MeshBuilder builder = new MeshBuilder();
		
		builder.startMeshBuild();
		
		for (int i = 0; i < triangleIndexers.size(); i++) {
			TriangleIndexer indexer = triangleIndexers.get(i);
			
			//Point 1
			putPoint(builder, vertices.get(((int) indexer.positionIndices.x) - 1), 
					 normals.get(((int) indexer.normalIndices.x) - 1), 
					 textureCoords.get(((int) indexer.textureIndices.x) - 1));
			
			//Point 2
			putPoint(builder, vertices.get(((int) indexer.positionIndices.y) - 1), 
					 normals.get(((int) indexer.normalIndices.y) - 1), 
					 textureCoords.get(((int) indexer.textureIndices.y) - 1));
			
			//Point 3
			putPoint(builder, vertices.get(((int) indexer.positionIndices.z) - 1), 
					 normals.get(((int) indexer.normalIndices.z) - 1), 
					 textureCoords.get(((int) indexer.textureIndices.z) - 1));
		}
		
		return builder.endMeshBuild();
	}
	
	private static void putPoint(MeshBuilder builder, Vector3f position, Vector3f normal, Vector2f texCoords) {
		builder.putVertex(new Vertex(position.x, position.y, position.z, normal.x, normal.y, normal.z, texCoords.x, texCoords.y));
	}
	
	
	public void render(Vector3f position, Vector3f rotation) {
		render(position.x, position.y, position.z, rotation.x, rotation.y, rotation.z);
	}
	
	public void render(Vector3f position) {
		render(position.x, position.y, position.z);
	}
	
	public void render(float x, float y, float z) {
		render(x, y, z, 0, 0, 0);
	}
	
	public void render(float x, float y, float z, float rx, float ry, float rz) { 
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_FLOAT, 8 * 4, 0);
		glNormalPointer(GL_FLOAT, 8 * 4, 3 * 4);
		glTexCoordPointer(2, GL_FLOAT, 8 * 4, 6 * 4);

		glPushMatrix();
		
		glTranslatef(x, y, z);
		glRotatef(rx, 1.0F, 0.0F, 0.0F);
		glRotatef(ry, 0.0F, 1.0F, 0.0F);
		glRotatef(rz, 0.0F, 0.0F, 1.0F);
		
		glDrawArrays(GL_TRIANGLES, 0, vertexCount);
		
		glPopMatrix();
		
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public ArrayList<Vertex> getVertices() {
		return vertices;
	}
	
	public int getVertexCount() {
		return vertexCount;
	}
	
	private static class TriangleIndexer {
		public Vector3f positionIndices = new Vector3f();
		public Vector3f normalIndices = new Vector3f();
		public Vector3f textureIndices = new Vector3f();
		
		@Override
		public String toString() {
			return positionIndices.x + "/" + normalIndices.x + "/" + textureIndices.x + " " + 
				   positionIndices.y + "/" + normalIndices.y + "/" + textureIndices.y + " " +
				   positionIndices.z + "/" + normalIndices.z + "/" + textureIndices.z;
		}
	}
}