package com.openglshaders.src.model;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;

public class MeshBuilder {
	private boolean buildingMesh = false;
	private ArrayList<Vertex> vertices = new ArrayList<Vertex>();
	private Vector3f origin = new Vector3f();
	
	public MeshBuilder() {
		
	}
	
	public MeshBuilder putVertex(Vertex vertex) {
		checkBuildingMesh();
		vertices.add(vertex);
		return this;
	}
	
	public MeshBuilder putMesh(Mesh mesh) {
		vertices.addAll(mesh.getVertices());
		return this;
	}
	
	public MeshBuilder startMeshBuild() {
		if (buildingMesh) {
			throw new IllegalStateException("Can't start a new mesh while you are already building one.");
		} else {
			buildingMesh = true;			
		}
		return this;
	}
	
	public Mesh endMeshBuild() {
		checkBuildingMesh();
		buildingMesh = false;	
		
		//Offset the vertices
		for (int i = 0; i < vertices.size(); i++) {
			vertices.get(i).vx -= origin.x;
			vertices.get(i).vy -= origin.y;
			vertices.get(i).vz -= origin.z;
		}
		
		Mesh mesh = new Mesh(vertices);

		vertices.clear();
		origin.set(0, 0, 0);
		return mesh;
	}
	
	public MeshBuilder setOrigin(Vector3f origin) {
		this.origin = origin;
		return this;
	}
	
	private void checkBuildingMesh() {
		if (!buildingMesh) {
			throw new IllegalStateException("Can't put a operate on a mesh that isn't started.");
		}
	}
	
	public static Mesh combineMeshes(Mesh mesh, Mesh mesh2) {
		ArrayList<Vertex> mesh1Vertices = mesh.getVertices();
		ArrayList<Vertex> mesh2Vertices = mesh2.getVertices();
		
		ArrayList<Vertex> tempVertices = new ArrayList<Vertex>();
		tempVertices.addAll(mesh1Vertices);
		tempVertices.addAll(mesh2Vertices);
		
		Mesh m = new Mesh(tempVertices);
		return m;
	}
}