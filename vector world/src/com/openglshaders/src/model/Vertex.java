package com.openglshaders.src.model;

public class Vertex {
	public float vx, vy, vz, nx, ny, nz, ts, tt = 0;
	
	public Vertex() {}
	
	public Vertex(float vx, float vy, float vz, float nx, float ny, float nz, float ts, float tt) {
		this.vx = vx;
		this.vy = vy;
		this.vz = vz;
		this.nx = nx;
		this.ny = ny;
		this.nz = nz;
		this.ts = ts;
		this.tt = tt;
	}
}