package com.javarpg.lr.game.world;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StreamTokenizer;

import com.javarpg.lr.game.world.wall.WallManager;
import com.javarpg.twod.texture.TextureManager;

/**
 * This is essentially just a tile manager..
 */
public class Region {//implements TileBasedMap {

	public static final int REGION_WIDTH = 42;
	public static final int REGION_HEIGHT = REGION_WIDTH;

	private final TextureManager textureManager;

	/*
	 * A list of tiles which makes up a region
	 */
	private Tile tiles[] = new Tile[REGION_WIDTH * REGION_HEIGHT];

	/*
	 * Indicator if a given tile has been visited during the search
	 */
	@SuppressWarnings("unused")
	private boolean[][] visited = new boolean[REGION_WIDTH][REGION_HEIGHT];

	public Region() {
		this.textureManager = TextureManager.getInstance();

		/*
		 * Initialize the tile list
		 */
		int tileId = 0;
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int y = 0; y < REGION_HEIGHT; y++) {
				tiles[tileId] = null;
				tiles[tileId] = new Tile(x, y);
				tileId++;
			}
		}
	}

	public void loadRegion(int regionX, int regionY) {
		loadTileData(regionX, regionY);
		WallManager.getInstance().reset();
		WallManager.getInstance().loadMapData(regionX, regionY);
	}

	private void loadTileData(int regionX, int regionY) {
		try {
			File map = new File("./assets/map_data/" + regionX + "-" + regionY + ".map");
			StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new FileReader(map)));
			while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				int type = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int x = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int y = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				boolean clipped = (int) tokenizer.nval == 1;
				Tile tile = getTile(x, y);
				if (tile == null) {
					System.err.println("[Tile] Map data is corrupt! Region: " + regionX + "-" + regionY);
					System.exit(1);
					continue;
				}
				tile.setType(type);
				tile.setClippingMode(clipped);

				switch (type) {
				case 8: // light water
				case 9: // dark water
				case 3: // magma
					//case 4: // null tile
					tile.setClippingMode(true);
					break;
				}
			}
			System.out.println("Loaded tile data for region " + regionX + "-" + regionY);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error loading tile data!");
			System.exit(1);
		}
	}

	public void renderTerrain(boolean grid) {
		for (int i = 0; i < tiles.length; i++) {
			textureManager.draw("tile" + tiles[i].getType(), tiles[i].getX() * 16, tiles[i].getY() * 16, 16, 16);
			if (grid) {
				textureManager.draw("tile", tiles[i].getX() * 16, tiles[i].getY() * 16);
			}
		}
	}

	public Tile getTile(int x, int y) {
		for (Tile tile : tiles) {
			if (tile.getX() == x && tile.getY() == y) {
				return tile;
			}
		}
		return null;
	}

	/**
	 * Returns the nearby tiles. Example:
	 * [0][1][2]
	 * [3][4][5]
	 * [6][7][8]
	 */
	public Tile[] getSurroundingTiles(int x, int y) {
		Tile tiles[] = new Tile[9];
		/*
		 * top row
		 */
		tiles[0] = getTile(x - 1, y - 1);
		tiles[1] = getTile(x, y - 1);
		tiles[2] = getTile(x + 1, y - 1);
		/*
		 * middle row
		 */
		tiles[3] = getTile(x - 1, y);
		tiles[4] = getTile(x, y); // probably the tile the player is standing on
		tiles[5] = getTile(x + 1, y);
		/*
		 * bottom row
		 */
		tiles[6] = getTile(x - 1, y + 1);
		tiles[7] = getTile(x, y + 1);
		tiles[8] = getTile(x + 2, y + 1);
		return tiles;
	}

	public Tile[] getTiles() {
		return tiles;
	}

	/**
	 * Clear the array marking which tiles have been visited by the path 
	 * finder.
	public void clearVisited() {
		for (int i = 0; i < tiles.length; i++) {
			visited[tiles[i].getX() / 16][tiles[i].getY() / 16] = false;
		}
	}

	@Override
	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}

	@Override
	public boolean blocked(Player player, int x, int y) {
		Tile tile = getTile(x, y);
		Prop prop = PropManager.getInstance().getProp(x, y);
		boolean blockedByProp = prop != null && prop.isClipped();
		return tile == null || tile.isClipped() || blockedByProp;
	}

	public float getCost(Player player, int sx, int sy, int tx, int ty) {
		return 1;
	}

	@Override
	public int getWidthInTiles() {
		return REGION_WIDTH;
	}

	@Override
	public int getHeightInTiles() {
		return REGION_HEIGHT;
	}
	*/

}