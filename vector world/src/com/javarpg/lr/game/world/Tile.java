package com.javarpg.lr.game.world;

/**
 * Represents a tile.
 */
public class Tile {

	private int x;
	private int y;
	private int type = 4;
	private boolean clipped;
	
	public Tile(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setType(int type) {
		this.type = type;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getType() {
		return type;
	}

	public void setClippingMode(boolean clipped) {
		this.clipped = clipped;
	}
	
	public boolean isClipped() {
		return clipped;
	}

}