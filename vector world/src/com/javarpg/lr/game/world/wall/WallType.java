package com.javarpg.lr.game.world.wall;

public enum WallType {
	
	BRICK_WALL, STONE_WALL, IRON_BLOCK, GOLD_BLOCK;
	
}