package com.javarpg.lr.game.world.wall;

import com.javarpg.twod.texture.TextureManager;

/**
 * Represents a wall.
 */
public class Wall {

	private final int type;
	private final int x;
	private final int y;
	
	public Wall(int type, int x, int y) {
		this.type = type;
		this.x = x;
		this.y = y;
	}

	public void render(TextureManager textureManager) {
		textureManager.draw(textureManager.getTexture("wall" + type).getName(), x * 16, y * 16);
	}
	
	public int getType() {
		return type;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}