package com.javarpg.lr.game.world.wall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StreamTokenizer;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.javarpg.twod.texture.TextureManager;

public class WallManager {

	private static final WallManager INSTANCE = new WallManager();
	private final TextureManager textureManager = TextureManager.getInstance();
	private List<Wall> wallList = new CopyOnWriteArrayList<Wall>();

	public WallManager() {
		// ..
	}

	public void loadMapData(int regionX, int regionY) {
		try {
			File map = new File("./assets/map_data/" + regionX + "-" + regionY + ".str");
			StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new FileReader(map)));
			while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				int type = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int x = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int y = (int) tokenizer.nval;
				Wall wall = new Wall(type, x, y);
				WallManager.getInstance().register(wall);
			}
			System.out.println("Loaded structural data for region " + regionX + "-" + regionY);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error loading wall data!");
			System.exit(1);
		}
	}

	/**
	 * Render the walls.
	 */
	public void render() {
		for (Wall wall : wallList) {
			wall.render(textureManager);
		}
	}

	public Wall getWall(int x, int y) {
		for (Wall wall : wallList) {
			if (wall.getX() == x && wall.getY() == y) {
				return wall;
			}
		}
		return null;
	}

	public void unregister(int x, int y) {
		Wall wall = getWall(x, y);
		unregister(wall);
	}

	public void unregister(Wall wall) {
		wallList.remove(wall);
	}

	public void register(Wall wall) {
		wallList.add(wall);
	}

	public void reset() {
		for (Wall wall : wallList) {
			wallList.remove(wall);
		}
	}

	public List<Wall> getWallList() {
		return wallList;
	}

	public static WallManager getInstance() {
		return INSTANCE;
	}

}