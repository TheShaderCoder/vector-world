package com.javarpg.lr.editor;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import com.javarpg.lr.game.world.Region;
import com.javarpg.lr.game.world.Tile;
import com.javarpg.lr.game.world.wall.Wall;
import com.javarpg.lr.game.world.wall.WallManager;
import com.javarpg.twod.Twod;
import com.javarpg.twod.texture.TextureManager;
import com.javarpg.twod.font.*;
import com.javarpg.twod.font.GLFont.FontType;
import com.javarpg.twod.util.Graphics;
import com.javarpg.twod.util.OSUtils;

/**
 * The map editor application.
 */
public class Editor extends Twod {

	private final TextureManager textureManager = TextureManager.getInstance();;

	private int snappedX;
	private int snappedY;
	@SuppressWarnings("unused")
	private int mouseX;
	@SuppressWarnings("unused")
	private int mouseY;

	private Tile clickedTile; // re-cycled variable

	private long lastMouseWheelTextureUpdate = 0L;

	private boolean tileEditor;
	private int tileType = 0;

	private boolean wallEditor;
	private int wallType = 0;

	private boolean clipEditor;

	private Region region;
	private WallManager wallManager;

	private int regionX = 0;
	private int regionY = 0;

	private int buttonId = -1;
	private final String button[] = { "New", "Save", "Delete" };

	public Editor() {
		super("World Editor", 673, 673, false, false, 60);
		Display.setResizable(false);

		textureManager.addTexture("tile", new File("./assets/tile.png"), true);
		
		/*
		 * Load tile textures
		 */
		boolean linear = false;
		for (int i = 0; i < 95; i++) {
			textureManager.addTexture("tile" + i, new File("./assets/tile/" + i + ".png"), linear);
		}

		/*
		 * Load wall textures
		 */
		textureManager.addTexture("wall0", new File("./assets/wall/brick_wall.png"), true);
		textureManager.addTexture("wall1", new File("./assets/wall/stone_wall.png"), true);
		textureManager.addTexture("wall2", new File("./assets/wall/iron_block.png"), true);
		textureManager.addTexture("wall3", new File("./assets/wall/gold_block.png"), true);


		this.wallManager = WallManager.getInstance();

		loadMapData(0, 0);

		super.run(17); // 17 = ~60ups
	}

	@Override
	public void handleResize(int width, int height) {
	}

	/**
	 * Process logic editor logic.
	 * @param currentTime The current system time in milliseconds.
	 */
	@Override
	public void processLogic(long currentTime) {
		Mouse.setGrabbed(tileEditor || wallEditor || clipEditor);
		int mouseWheel = Mouse.getDWheel();
		if (mouseWheel > 0) {
			if (tileType < 99) {
				tileType++;
			}
			if (wallType < 3) {
				wallType++;
			}
		} else if (mouseWheel < 0) {
			if (tileType > 0) {
				tileType--;
			}
			if (wallType > 0) {
				wallType--;
			}
		}
	}

	/**
	 * Renders the editor state contents.
	 */
	@Override
	public void render2D() {
		if (region != null) {
			region.renderTerrain(false);
		}

		wallManager.render();

		if (tileEditor) {
			textureManager.draw("tile" + tileType, snappedX * 16, snappedY * 16, 16, 16);
			textureManager.draw("tile", snappedX * 16, snappedY * 16);
		}
		if (wallEditor) {
			textureManager.draw("wall" + wallType, snappedX * 16, snappedY * 16, 16, 16);
			textureManager.draw("tile", snappedX * 16, snappedY * 16);
		}
	}

	/**
	 * Draw Graphics / GLStrings.
	 */
	@Override
	public void renderOverlay(int lastFrameCount, int lastUpdateCount) {
		/*
		 * Draw text
		 */
		Graphics.setColor(Color.YELLOW);
		GLFont.getInstance().drawString("FPS: " + lastFrameCount + ", UPS: " + lastUpdateCount, 15, 15, 1, 1, 0, "Arial");
		GLFont.getInstance().drawString("Mouse: x" + snappedX + ", y" + snappedY, 15, 30, 1, 1, 0, "Arial");
		GLFont.getInstance().drawString("Current Region: " + regionX + "-" + regionY , 15, 45, 1, 1, 0, "Arial");

		if (clipEditor) {
			Graphics.setColor(Color.RED, .50f);
			/*
			 * Draw all clipped tiles
			 */
			for (Tile tile : region.getTiles()) {
				if (tile.isClipped()) {
					Graphics.drawBox(tile.getX() * 16, tile.getY() * 16, 16);
				}
			}

			/*
			 * Draw hovering tile
			 */
			Graphics.drawBox(snappedX * 16, snappedY * 16, 16);
			GL11.glEnable(GL11.GL_TEXTURE_2D); // Begin sprite rendering.
			textureManager.draw("tile", snappedX * 16, snappedY * 16);
			GL11.glDisable(GL11.GL_TEXTURE_2D); // End sprite rendering.
		}

		Graphics.setColor(Color.WHITE, .50f);
		GLFont.getInstance().drawString("JavaRPG.com", Display.getWidth() - 100, Display.getHeight() - 25, 1, 1, 0, "Arial");

		for (int i = 0; i < 3; i++) {
			Graphics.setColor(Color.WHITE, .50f);
			Graphics.drawRectangle(Display.getWidth() - 80, 5 + (i * 30), 75, 25);

			Graphics.setColor(buttonId == i ? Color.GREEN : Color.ORANGE);
			GLFont.getInstance().drawString(button[i], Display.getWidth() - 80 + 10, 5 + (i * 30) + 5, 1, 1, 0, "Arial");
		}
	}

	/**
	 * Polls keyboard input which is used locally for the editor state.
	 */
	@Override
	public void pollKeyboard(int keyCode, boolean released, boolean repeatEvent) {
		switch (keyCode) {
		case Keyboard.KEY_ESCAPE:
			System.out.println("Application terminated by user.");
			super.shutdown();
			break;
		case Keyboard.KEY_T:
			if (released) {
				if (tileEditor) {
					tileEditor = false;
					return;
				}
				resetEditorMode();
				tileEditor = true;
			}
			break;
		case Keyboard.KEY_C:
			if (released) {
				if (clipEditor) {
					clipEditor = false;
					return;
				}
				resetEditorMode();
				clipEditor = true;
			}
			break;
		case Keyboard.KEY_W:
			if (released) {
				if (wallEditor) {
					wallEditor = false;
					return;
				}
				resetEditorMode();
				wallEditor = true;
			}
			break;
		case Keyboard.KEY_P:// P = plant editor
		case Keyboard.KEY_O:// O = ore editor
			break;
		}
	}

	/**
	 * Polls mouse input that will be used for the editor state.
	 * @param mouseX The 2D X mouse coordinate
	 * @param mouseY The 2D Y mouse coordinate
	 */
	@Override
	public void pollMouse(int mouseX, int mouseY) {
		int mouseEventButton = Mouse.getEventButton();
		boolean mouseButtonPressed = Mouse.getEventButtonState();

		this.mouseX = mouseX;
		this.mouseY = mouseY;
		this.snappedX = (mouseX / 16);
		this.snappedY = (mouseY / 16);

		buttonId = -1;
		if (!tileEditor && !wallEditor && !clipEditor) {
			for (int i = 0; i < button.length; i++) {
				if (mouseX > Display.getWidth() - 80 && mouseX < Display.getWidth() - 5 && mouseY > 5 + (i * 30) && mouseY < 25 + (i * 25)) {
					buttonId = i;
				}
			}
		}

		if (mouseEventButton == 0 && mouseButtonPressed) {
			if (buttonId > -1) {
				switch (buttonId) {
				case 0: // create
					break;
				case 1: // save
					saveMapData();
					break;
				case 2: // delete
					//deleteMapData();
					break;
				}
			}
		}
		/*
		 * Get the interacting tile
		 */
		if (mouseEventButton == 0 || mouseEventButton == 1) {
			clickedTile = region.getTile(snappedX, snappedY);
			if (clickedTile == null) {
				System.err.println("Mouse -> Tile is null!");
				return;
			}
			/*
			 * If the tile is not null, let's process the event
			 */
			switch (mouseEventButton) {
			case 0:
				// Left mouse button pressed
				if (mouseButtonPressed) {
					if (tileEditor) { // update tile texture
						clickedTile.setType(tileType);
					}
					if (wallEditor) { // place a wall
						Wall wall = new Wall(wallType, snappedX, snappedY);
						wallManager.register(wall);
					}
					if (clipEditor) { // enable tile clipping
						clickedTile.setClippingMode(true);
					}
				}
				break;
			case 1:
				// Right mouse button pressed
				if (mouseButtonPressed) {
					if (wallEditor) {
						wallManager.unregister(snappedX, snappedY);
					}
					if (clipEditor) { // disable tile clipping
						clickedTile.setClippingMode(false);
					}
				}
				break;
			}
		}
	}

	/**
	 * Resets the editor state.
	 */
	private void resetEditorMode() {
		wallEditor = false;
		tileEditor = false;
		clipEditor = false;
	}

	/**
	 * Loads the map data and implements it into the current session.
	 * @param regionX
	 * @param regionY
	 */
	private void loadMapData(int regionX, int regionY) {
		this.regionX = regionX;
		this.regionY = regionY;

		File map = new File("./assets/map_data/" + regionX + "-" + regionY + ".map");
		if (!map.exists()) {
			generateDefaultTileData(map);
		}

		try {
			map = new File("./assets/map_data/" + regionX + "-" + regionY + ".str");
			if (!map.exists()) {
				map.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * Initialize the region instance and load a region
		 * so the background isn't empty (black space)
		 */
		this.region = new Region();
		region.loadRegion(regionX, regionY);
	}

	/**
	 * Generates an empty map data file.
	 */
	private void generateDefaultTileData(File map) {
		try {
			map.createNewFile();
			PrintWriter pw = new PrintWriter(new FileOutputStream(map));
			pw.println("// default map: " + regionX + "-" + regionY);
			int tileId = 0;
			Tile tiles[] = new Tile[Region.REGION_WIDTH * Region.REGION_HEIGHT];
			for (int x = 0; x < Region.REGION_WIDTH; x++) {
				for (int y = 0; y < Region.REGION_HEIGHT; y++) {
					tiles[tileId] = null;
					tiles[tileId] = new Tile(x, y);
					pw.println("4, " + tiles[tileId].getX() + ", " + tiles[tileId].getY() + ", 0");
				}
				tileId++;
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error creating map data! File: " + map.getName());
			System.exit(1);
		}
	}

	/**
	 * Saves the current session's map data as a software-readable file.
	 */
	private void saveMapData() {
		File tileMap = new File("./assets/map_data/" + regionX + "-" + regionY + ".map");
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(tileMap));
			pw.println("// region " + regionX + "-" + regionY);
			Tile tiles[] = region.getTiles();
			for (int i = 0; i < tiles.length; i++) {
				textureManager.draw("tile" + tiles[i].getType(), tiles[i].getX() * 16, tiles[i].getY() * 16);
				pw.println(tiles[i].getType() + ", " + tiles[i].getX() + ", " + tiles[i].getY() + ", " + (tiles[i].isClipped() ? "1" : "0"));
			}
			pw.close();
			System.out.println("Saved map data for region " + regionX + "-" + regionY);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("Unable to produce map data file: " + tileMap.getName());
			System.exit(1);
		}
		File structureMap = new File("./assets/map_data/" + regionX + "-" + regionY + ".str");
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(structureMap));
			pw.println("// region " + regionX + "-" + regionY);
			List<Wall> walls = WallManager.getInstance().getWallList();
			for (Wall wall : walls) {
				pw.println(wall.getType() + ", " + wall.getX() + ", " + wall.getY());
			}
			pw.close();
			System.out.println("Saved structural data for region " + regionX + "-" + regionY);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("Unable to produce map data file: " + structureMap.getName());
			System.exit(1);
		}
	}

	/**
	 * Deletes all random generated map files.
	 */
	private void deleteMapData() {
		File[] files = new File("./assets/map_data/").listFiles();
		for (File file : files) {
			if (file.isFile() && file.getName().startsWith(regionX + "-" + regionY)) {
				file.delete();
				System.out.println("Deleted map: " + file.getName());
			}
		}
	}

	public static void main(String[] args) {
		/*
		 * Load native library
		 */
		OSUtils.loadNatives("./assets/");

		/*
		 * Create the game instance
		 */
		new Editor();
	}


}
