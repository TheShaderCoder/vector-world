import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import com.javarpg.twod.texture.Texture;
import com.javarpg.twod.texture.TextureManager;
import com.openglshaders.src.model.Mesh;
import com.openglshaders.src.object.ShapeGenerator;

public class Player {
	
	private final float MOVEMENT_SPEED = .080f;
	
	private Vector3f position = new Vector3f(); // (absolute position)
	private Vector3f previous = new Vector3f(); // last known position
	private Vector3f rotation = new Vector3f();

	private boolean moveForward = false;
	private boolean moveBackward = false;
	private boolean isSprinting = false;
	
	private Texture texture;

	private float height = 5f;
	private float width = 0.1f;
	private float length = 2;
	private static Mesh playerMesh = null;
	
	public Player(Vector3f position) {
		this.position = position;
		this.texture = TextureManager.getInstance().getTexture("human");
		if (playerMesh == null) {
			playerMesh = ShapeGenerator.createBoxMesh(new Vector3f(0, -2.5F, 0), new Vector3f(width, height, length));
		}
	}

	public void render() {
		texture.bind();
		playerMesh.render(position, rotation);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public void handleInput(boolean[] keys) {
		if (keys[Keyboard.KEY_W]) {
			moveForward = true;
		} else {
			moveForward = false;
		}
		if (keys[Keyboard.KEY_S]) {
			moveBackward = true;
		} else {
			moveBackward = false;
		}
		if (keys[Keyboard.KEY_A]) {
			rotation.y -= 2.2f;
		}
		if (keys[Keyboard.KEY_D]) {
			rotation.y += 2.2f;
		}
		if (keys[Keyboard.KEY_UP]) {
			position.y += .2f;
		}
		if (keys[Keyboard.KEY_DOWN]) {
			position.y -= .2f;
		}
		isSprinting = false;
		if (keys[Keyboard.KEY_LSHIFT] || keys[Keyboard.KEY_RSHIFT] || keys[Keyboard.KEY_LCONTROL] || keys[Keyboard.KEY_RCONTROL]) {
			isSprinting = true;
		}
		move();
	}

	public void move() {
		/*
		 * Update previous vector
		 */
		previous.x = position.x;
		previous.y = position.y;
		previous.z = position.z;

		float speed = isSprinting ? MOVEMENT_SPEED * 2 : MOVEMENT_SPEED;
		if (moveForward) {
			position.x += Math.sin(rotation.y * Math.PI / 180) * speed;
			position.z += -Math.cos(rotation.y * Math.PI / 180) * speed;
		}
		if (moveBackward) {
			position.x -= Math.sin(rotation.y * Math.PI / 180) * speed;
			position.z -= -Math.cos(rotation.y * Math.PI / 180) * speed;
		}
	}

	public boolean positionUpdateFlag() {
		if (previous != position) {
			return true;
		}
		return false;
	}


	public Vector3f getPosition() {
		return position;
	}

	public int getX() {
		return (int) Math.floor(position.x);
	}

	public int getZ() {
		return (int) Math.floor(position.z);
	}

	public float getHeight() {
		return position.y;
	}

	public void translate(float positionX, float positionY, float positionZ, float rotationX, float rotationY, float rotationZ) {
		this.position = new Vector3f(positionX, positionY, positionZ);
		this.rotation = new Vector3f(rotationX, rotationY, rotationZ);
	}
	
}