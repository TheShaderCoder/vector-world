import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 * The free roam camera.
 */
public class Camera {

	private final float MOVEMENT_SPEED = .080f;

	private Vector3f position = new Vector3f(); // (absolute position)
	private Vector3f rotation = new Vector3f();

	private boolean moveForward = false;
	private boolean moveBackward = false;
	private boolean moveLeft = false;
	private boolean moveRight = false;
	private boolean isSprinting = false;

	public Camera() {
		this.position = new Vector3f(0, 2, 0);
	}

	public void bind() {
		GL11.glRotatef(rotation.x, 1, 0, 0);
		GL11.glRotatef(rotation.y, 0, 1, 0);
		GL11.glRotatef(rotation.z, 0, 0, 1);
		GL11.glTranslatef(-position.x, -position.y, -position.z);
	}

	public void handleInput(boolean[] keys) {
		if (keys[Keyboard.KEY_W]) {
			moveForward = true;
		} else {
			moveForward = false;
		}
		if (keys[Keyboard.KEY_S]) {
			moveBackward = true;
		} else {
			moveBackward = false;
		}
		if (keys[Keyboard.KEY_A]) {
			moveLeft = true;
		} else {
			moveLeft = false;
		}
		if (keys[Keyboard.KEY_D]) {
			moveRight = true;
		} else {
			moveRight = false;
		}
		if (keys[Keyboard.KEY_LEFT]) {
			rotation.y -= 2.2f;
		}
		if (keys[Keyboard.KEY_RIGHT]) {
			rotation.y += 2.2f;
		}
		if (keys[Keyboard.KEY_UP]) {
			if (rotation.x - 2.2f < -79) {
				rotation.x = -79;
			} else {
				rotation.x -= 2.2f;
			}
		}
		if (keys[Keyboard.KEY_DOWN]) {
			if (rotation.x + 2.2f > 79) {
				rotation.x = 79;
			} else {
				rotation.x += 2.2f;
			}
		}
		if (keys[Keyboard.KEY_Q]) {
			position.y += .2f;
		}
		if (keys[Keyboard.KEY_R]) {
			position.y -= .2f;
		}
		isSprinting = false;
		if (keys[Keyboard.KEY_LSHIFT] || keys[Keyboard.KEY_RSHIFT] || keys[Keyboard.KEY_LCONTROL] || keys[Keyboard.KEY_RCONTROL]) {
			isSprinting = true;
		}
		if (keys[Keyboard.KEY_P]) {
			System.out.println("position: " + position.x + ", " + position.y + ", " + position.z);
			System.out.println("rotation: " + rotation.x + ", " + rotation.y + ", " + rotation.z);
		}
		move();
	}

	public void move() {
		float speed = isSprinting ? MOVEMENT_SPEED * 2 : MOVEMENT_SPEED;
		if (moveForward) {
			position.x += Math.sin(rotation.y * Math.PI / 180) * speed;
			position.z += -Math.cos(rotation.y * Math.PI / 180) * speed;
		}
		if (moveBackward) {
			position.x -= Math.sin(rotation.y * Math.PI / 180) * speed;
			position.z -= -Math.cos(rotation.y * Math.PI / 180) * speed;
		}
		if (moveLeft) {
			position.x += Math.sin((rotation.y - 90) * Math.PI / 180) * speed;
			position.z += -Math.cos((rotation.y - 90) * Math.PI / 180) * speed;
		}
		if (moveRight) {
			position.x += Math.sin((rotation.y + 90) * Math.PI / 180) * speed;
			position.z += -Math.cos((rotation.y + 90) * Math.PI / 180) * speed;
		}
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public Vector3f getPosition() {
		return position;
	}

	public int getX() {
		return (int) Math.floor(position.x);
	}

	public int getZ() {
		return (int) Math.floor(position.z);
	}

	public float getHeight() {
		return position.y;
	}

	public void translate(float positionX, float positionY, float positionZ, float rotationX, float rotationY, float rotationZ) {
		this.position = new Vector3f(positionX, positionY, positionZ);
		this.rotation = new Vector3f(rotationX, rotationY, rotationZ);
	}

}