import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import com.javarpg.twod.texture.Texture;
import com.javarpg.twod.texture.TextureManager;
import com.openglshaders.src.ResourceList;
import com.openglshaders.src.object.ShapeGenerator;

/**
 * Represents a wall, which is composed of blocks.
 */
public class Wall {
	
	private final Vector3f render; // render coords (start position)
	private int type;
	private Texture texture;

	/*
	 * The scale of a single block.
	 */
	private final float SCALE = 2;
	private int height = 3; // the height (in blocks) of this wall
	private float width = SCALE;
	private float length = SCALE;
	
	public Wall(int type, float startX, float startZ) {
		this.render = new Vector3f(startX, 0, startZ);
		setTexture(type);
		if (!ResourceList.hasMesh("wall")) {
			ResourceList.addMesh("wall", ShapeGenerator.createBoxMesh(new Vector3f(-1, -1, -1), new Vector3f(2, 2, 2)));
		}
	}

	public void setTexture(int type) {
		this.type = type;
		this.texture = TextureManager.getInstance().getTexture("wall" + type);
	}

	public void render() {
		texture.bind();
		for (int i = 0; i < height; i++) {
			ResourceList.getMesh("wall").render(render.x, render.y + i * SCALE, render.z);
		}
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public int getType() {
		return type;
	}
	
	public Vector3f getRenderPosition() {
		return render;
	}
}