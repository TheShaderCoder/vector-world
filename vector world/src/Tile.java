import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.javarpg.twod.texture.Texture;
import com.javarpg.twod.texture.TextureManager;
import com.openglshaders.src.ResourceList;
import com.openglshaders.src.object.ShapeGenerator;

/**
 * Represents a tile.
 * TODO Convert to triangles, for a 'smoother' look?
 */
public class Tile {

	private final Vector3f render; // render coords (start position)

	private int type;
	private Texture texture;
	private boolean clipped;
	
	public Tile(int type, float startX, float startZ) {
		this.render = new Vector3f(startX, 0, startZ);
		setTexture(type);
		if (!ResourceList.hasMesh("tile")) {
			ResourceList.addMesh("tile", ShapeGenerator.createPlaneMesh(new Vector3f(), new Vector2f(2, 2)));			
		}
	}

	public void setTexture(int type) {
		this.type = type;
		this.texture = TextureManager.getInstance().getTexture("tile" + type);
	}

	public void render() {
		texture.bind();
		ResourceList.getMesh("tile").render(render.x, render.y, render.z);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}

	public int getType() {
		return type;
	}
	
	public Vector3f getRenderPosition() {
		return render;
	}

	public void setClippingMode(boolean clipped) {
		this.clipped = clipped;
	}
	
	public boolean isClipped() {
		return clipped;
	}
}