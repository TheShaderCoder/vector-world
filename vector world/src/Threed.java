import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StreamTokenizer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.javarpg.twod.texture.Texture;
import com.javarpg.twod.texture.TextureManager;
import com.openglshaders.src.ResourceList;
import com.openglshaders.src.object.Cone;
import com.openglshaders.src.object.Cube;
import com.openglshaders.src.object.Cylinder;
import com.openglshaders.src.object.Object3D;
import com.openglshaders.src.object.Plane;
import com.openglshaders.src.object.ShapeGenerator;
import com.openglshaders.src.object.Sphere;
import com.openglshaders.src.physics.AxisWidget;
import com.openglshaders.src.physics.DebugRenderer;
import com.openglshaders.src.physics.MatrixMath;
import com.openglshaders.src.physics.Physics;
import com.openglshaders.src.physics.Ray;
import com.openglshaders.src.physics.RaycastHit;

/**
 * Loads "Lone Ranger" 2D map data, and renders it in a simple 3D world.
 */
public class Threed {

	private TextureManager textureManager;
	private Texture white;
	private final Camera camera;
	private final Player player;
	private boolean[] keys = new boolean[256];
	private List<Tile> tileCache = new ArrayList<Tile>();
	private List<Wall> wallCache = new ArrayList<Wall>();
	private boolean keepAlive = false;
	private boolean wireFrame = false;
	private boolean cameraUnlocked = false;

	private boolean foggingEnabled = false;
	private float fogColors[] = { 0.025f, 0.1f, 0.05f, 0.01f };

	private long lastCycle = 0L;
	private String title = "Lone Ranger 3D Render";
	private int frameCount[] = { 0, 0 };
	private AxisWidget widget;
	private Ray lastRay;
	private Object3D testObjectCone;
	private Object3D testObjectCube;
	private Sphere testObjectSphere;
	private Object3D testObjectPlane;
	private Object3D testObjectCylinder;
	private Sphere raySphere;
	
	
	public static void main(String[] args) {
		System.setProperty("org.lwjgl.librarypath", new File("./assets/native/macosx").getAbsolutePath());
		new Threed();
	}

	public Threed() {
		try {
			Display.setTitle(title);
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setResizable(true);
			Display.setVSyncEnabled(true);
			Display.create();

			init3D();
			init2D();

			Mouse.create();
			Keyboard.create();
			Keyboard.enableRepeatEvents(true); // Disabled by default
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

		this.camera = new Camera();
		camera.translate(40, 30, 70, 31.85f, -360, 0);
		
		this.player = new Player(new Vector3f(32, 0, 32));

		this.keepAlive = true;
		while (keepAlive && !Display.isCloseRequested()) {
			/*
			 * Process any logic that should be updated even when
			 * the application is not in focus.
			 */
			processLogic();

			/*
			 * If the application is in focus, render the contents
			 * and poll user input.
			 */
			if (Display.isActive()) {
				preRender3D();
				render3D();
				preRender2D();
				render2D();
				pollMouse();
				pollKeyboard();
				frameCount[0]++;
			}
			Display.update();
			Display.sync(30);
		}
		Keyboard.destroy();
		Display.destroy(); // note: this also calls Mouse.destroy()
		System.exit(0);
	}

	private void processLogic() {
		long currentTime = (Sys.getTime() * 1000) / Sys.getTimerResolution();
		if (lastCycle < currentTime - 999) {
			lastCycle = currentTime;
			frameCount[1] = frameCount[0];
			frameCount[0] = 0;
			Display.setTitle(title + " (fps: " + frameCount[1] + ", mem: " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) >> 20) + ")");
		}
	}

	private void pollMouse() {
		while (Mouse.next()) {
			int mouseEventButton = Mouse.getEventButton();
			boolean mouseButtonPressed = Mouse.getEventButtonState();

			switch (mouseEventButton) {
			case 0:
				// Left mouse button pressed
				if (mouseButtonPressed) {
				}
				break;
			case 1:
				// Right mouse button pressed
				if (mouseButtonPressed) {
					Mouse.setGrabbed(!Mouse.isGrabbed());
				}
				break;
			}
		}
	}

	private void pollKeyboard() {
		while (Keyboard.next()) {
			int keyCode = Keyboard.getEventKey();
			boolean released = Keyboard.getEventKeyState();
			switch (keyCode) {
			case Keyboard.KEY_ESCAPE:
				keepAlive = false;
				break;
			case Keyboard.KEY_C:
				if (released) {
					cameraUnlocked = !cameraUnlocked;
				}
				break;
			case Keyboard.KEY_F:
				if (released) {
					wireFrame = !wireFrame;
				}
				if (wireFrame) {
					GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
					GL11.glDisable(GL11.GL_TEXTURE_2D);
				} else {
					GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
					GL11.glEnable(GL11.GL_TEXTURE_2D);
				}
				System.out.println("wireFrame="+wireFrame);
				break;
			case Keyboard.KEY_F1:
				ResourceList.setMesh("tile", ShapeGenerator.createPlaneMesh(new Vector3f(), new Vector2f(2, 2)));
				ResourceList.setMesh("wall", ShapeGenerator.createBoxMesh(new Vector3f(-1, -1, -1), new Vector3f(2, 2, 2)));

				
				break;
			default:
				for (int i = 0 ; i < 256; i++) {
					keys[i] = Keyboard.isKeyDown(i);
				}
				break;
			}
		}
		if (cameraUnlocked) {
			camera.handleInput(keys);
		} else {
			player.handleInput(keys);
		}
	}

	private void init3D() {
		GL11.glClearColor(fogColors[0], fogColors[1], fogColors[2], 0); // background color
		GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables depth testing
		GL11.glClearDepth(2.5f); // Clear depth buffer
		GL11.glDepthFunc(GL11.GL_LEQUAL); // Sets the type of test to use for depth testing

		/*
		 * Load all of the game textures from the Lone Ranger asset storage
		 */
		this.textureManager = TextureManager.getInstance();
		textureManager.addTexture("white", Color.WHITE);
		this.white = textureManager.getTexture("white");

		boolean linear = false;
		for (int i = 0; i < 95; i++) {
			try {
				textureManager.addTexture("tile" + i, new File("./assets/tile/" + i + ".png"), linear);
				System.out.println("loaded tile: " + i);
				Thread.sleep(1);
			} catch (Exception e) {
				System.err.println("Error reading tile texture: " + i);
			}
		}

		textureManager.addTexture("wall0", new File("./assets/wall/brick_wall.png"), linear);
		textureManager.addTexture("wall1", new File("./assets/wall/stone_wall.png"), linear);
		textureManager.addTexture("wall2", new File("./assets/wall/iron_block.png"), linear);
		textureManager.addTexture("wall3", new File("./assets/wall/gold_block.png"), linear);

		/*
		 * Load the Lone Ranger map data
		 */
		loadTileData();
		loadWallData();

		/*
		 * Load all of the Threed textures..
		 */
		textureManager.addTexture("cursor", new File("./assets/threed/cursor.png"));
		textureManager.addTexture("human", new File("./assets/threed/npc.jpg"));

		/*
		 * Initialize fogging effect
		 */
		if (foggingEnabled) {
			GL11.glEnable(GL11.GL_FOG);
			FloatBuffer fogColor = BufferUtils.createFloatBuffer(4);
			fogColor.put(fogColors[0]).put(fogColors[1]).put(fogColors[2]).put(fogColors[3]).flip();
			GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_LINEAR);
			GL11.glFog(GL11.GL_FOG_COLOR, fogColor);
			GL11.glHint(GL11.GL_FOG_HINT, GL11.GL_NICEST);
			GL11.glFogf(GL11.GL_FOG_START, 10);
			GL11.glFogf(GL11.GL_FOG_END, 50);
		}

		/*
		 * Loads some assets
		 */
		ResourceList.load();
		widget = new AxisWidget(Physics.up, Physics.forward, Physics.right);
		lastRay = new Ray(new Vector3f(), new Vector3f(1, 0, 0));
		testObjectCone = new Cone(new Vector3f(), 20, 1, 2);
		testObjectCube = new Cube(new Vector3f(0, -1F, 0), new Vector3f(2, 2, 2));
		testObjectPlane = new Plane(new Vector3f(), new Vector2f(2, 2));
		testObjectSphere = new Sphere(new Vector3f(), 10, 4);
		testObjectCylinder = new Cylinder(new Vector3f(), 1, 4, 10);
		
		raySphere = new Sphere(new Vector3f(), 1, 2);
		
		loadWallData();
	}

	public void preRender3D() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();

		if (Display.wasResized()) {
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
		}
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		float fovy = 90;
		float aspect = Display.getWidth() / (float) Display.getHeight();
		float zNear = 0.1f;
		float zFar = 100.0f;
		GLU.gluPerspective(fovy, aspect, zNear, zFar);

		glMatrixMode(GL_MODELVIEW);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); 
		glLoadIdentity();

		GL11.glDisable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE_MINUS_CONSTANT_COLOR);

		GL11.glColor3f(1, 1, 1); // reset color
		
		GL11.glCullFace(GL11.GL_FRONT);
		GL11.glEnable(GL11.GL_CULL_FACE);
	}

	public void render3D() {
		ResourceList.getShader("tex_shader").bind();

		if (!wireFrame) {
			GL11.glEnable(GL11.GL_TEXTURE_2D);
		}

		camera.bind();

		for (Tile tile : tileCache) {
			tile.render();
		}

		/*
		 * Draw walls
		 */
		for (Wall wall : wallCache) {
			wall.render();
		}
		
		player.render();

		if (!wireFrame) {
			GL11.glDisable(GL11.GL_TEXTURE_2D);
		}
		
		ResourceList.getShader("tex_shader").unbind();

		
		//Debug rendering
		widget.draw();
		ResourceList.getMesh("cube").render(100, 10, 100);
		testObjectCone.getMesh().render(40, 0, 40);
		testObjectCube.getMesh().render(44, 0, 40);
		testObjectPlane.getMesh().render(48, 0, 40);
		testObjectSphere.getMesh().render(52, 0, 40);
		testObjectCylinder.getMesh().render(56, 0, 40);
		
		Vector3f rotationVec = new Vector3f(Physics.forward);
		Vector3f camRot = new Vector3f(camera.getRotation());
		
		Matrix3f rotationMatrix = MatrixMath.rotationMatrixFromEulerAngles(camRot);
		rotationVec.set(rotationMatrix.m02, rotationMatrix.m12, -rotationMatrix.m22);

		if (Mouse.isButtonDown(0)) {
			lastRay = new Ray(new Vector3f(camera.getPosition()), new Vector3f(rotationVec));
		}
		
		Vector3f p0 = new Vector3f(100, 0, 0);
		Vector3f p1 = new Vector3f(100, 50, 50);
		Vector3f p2 = new Vector3f(100, 0, 50);

		RaycastHit hit = new RaycastHit(false, new Vector3f());
		Physics.testRayTriangle(lastRay, p0, p1, p2, hit);
		
		DebugRenderer.drawDebugTriangle(p0, p1, p2, hit.hit);
		
		DebugRenderer.drawRay(lastRay);
		DebugRenderer.drawPoint(hit.hitLocation);
		
		if (Physics.testRaySphere(lastRay, raySphere, hit)) {
			GL11.glColor3f(0.0F, 1.0F, 0.0F);
		} else {
			GL11.glColor3f(1.0F, 0.0F, 0.0F);
		}

		raySphere.getMesh().render(0, 0, 0);
		GL11.glColor3f(1.0F, 1.0F, 1.0F);
		
		DebugRenderer.drawPoint(hit.hitLocation);
	}

	private void init2D() {
		// TODO Auto-generated method stub

	}

	private void preRender2D() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, -1, 1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_CULL_FACE);
		
		/*
		 * Next, we need to enable blending. Without this enabled, transparent sprites will not work.
		 */
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		white.bind(); // so that 2D colors wont mix

	}

	private void render2D() {
		if (Mouse.isGrabbed()) {
			textureManager.draw("cursor", getMouseX() - 32, getMouseY() - 32);
		}
	}

	private void loadTileData() {
		try {
			File map = new File("./assets/map_data/0-0.map");
			StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new FileReader(map)));
			while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				int type = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int x = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int y = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				boolean clipped = (int) tokenizer.nval == 1;

				Tile tile = new Tile(0, x * 2, y * 2);
				tile.setTexture(type);

				switch (type) {
				case 8: // light water
				case 9: // dark water
				case 3: // magma
					//case 4: // null tile
					tile.setClippingMode(true);
					break;
				default:
					tile.setClippingMode(clipped);
					break;
				}
				tileCache.add(tile);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error loading tile data!");
			System.exit(1);
		}
	}

	public void loadWallData() {
		try {
			File map = new File("./assets/map_data/0-0.str");
			StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new FileReader(map)));
			while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				int type = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int x = (int) tokenizer.nval;
				tokenizer.nextToken();
				tokenizer.nextToken();
				int y = (int) tokenizer.nval;
				Wall wall = new Wall(type, x * 2, y * 2);
				wallCache.add(wall);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error loading wall data!");
			System.exit(1);
		}
	}

	public static int getMouseX() {
		return Mouse.getX();
	}

	public static int getMouseY() {
		return Display.getHeight() - Mouse.getY();
	}

}